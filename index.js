require('dotenv').config();

const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const initialiseData = require('./initial-data');

const isProd = process.env.NODE_ENV === 'production';
const PROJECT_NAME = 'Catalejo';
const adapterConfig = {
	mongoUri: process.env.MONGODB_URI,
	useNewUrlParser: true,
	useFindAndModify: false,
	useUnifiedTopology: true,
};

const sessionStore = new MongoStore({
	url: process.env.MONGODB_URI,
	autoReconnect: true,
	secret: process.env.SECRET,
	mongooseConnection: adapterConfig,
	autoRemove: 'disabled',
});

const keystone = new Keystone({
	name: PROJECT_NAME,
	adapter: new Adapter(adapterConfig),
	onConnect: initialiseData,
	sessionStore: sessionStore,
	cookieSecret: process.env.COOKIE_SECRET,
	secureCookies: isProd,
});

//Schemas
require('./server/models')(keystone);

const authStrategy = keystone.createAuthStrategy({
	type: PasswordAuthStrategy,
	list: 'User',
});

module.exports = {
	keystone,
	apps: [
		new GraphQLApp(),
		new AdminUIApp({
			name: PROJECT_NAME,
			enableDefaultRoute: true,
			authStrategy,
			hooks: require.resolve('./server/admin/'),
		}),
	],
	configureExpress: (app) => {
		if (isProd) {
			app.set('trust proxy', 1);
		}
	},
};
