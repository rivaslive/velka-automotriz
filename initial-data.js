module.exports = async (keystone) => {
	// Count existing users
	const {
		data: {
			_allUsersMeta: { count = 0 },
		},
	} = await keystone.executeGraphQL({
		context: keystone.createContext().sudo(),
		query: `query {
      _allUsersMeta {
        count
      }
    }`,
	});

	if (count === 0) {
		const email = process.env.EMAIL_INITIAL_DATA;
		const password = process.env.PASSWORD_INITIAL_DATA;

		const { errors } = await keystone.executeGraphQL({
			context: keystone.createContext().sudo(),
			query: `mutation initialUser($password: String, $email: String) {
            createUser(data: {name: "Admin", email: $email, isAdmin: true, password: $password, isAdmin: true}) {
              id
            }
          }`,
			variables: { password, email },
		});

		if (errors) {
			console.log('failed to create initial Avatar:');
			console.log(errors);
		} else {
			console.log(`
        User created:
        email: ${email}
        password: ${password}
        Please change these details after initial login.
      `);
		}
	}
};
