import { HttpLink, ApolloClient, InMemoryCache } from '@apollo/client';

const isProd = process.env.NODE_ENV === 'production';

const client = new ApolloClient({
	link: new HttpLink({
		uri: `${
			isProd
				? 'https://velka-automotriz.herokuapp.com'
				: 'http://localhost:3000'
		}/admin/api`,
	}),
	cache: new InMemoryCache(),
});

export default client;
