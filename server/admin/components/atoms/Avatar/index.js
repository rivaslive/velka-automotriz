import React from 'react';
import ReactAvatar from 'react-avatar';
import { StyleAvatar, StyleAvatarContent } from './style';

const Avatar = ({
	name,
	id,
	active,
	isList,
	onClick,
	size,
	...restReactAvatar
}) => {
	// handlers
	const onHandler = React.useCallback(() => {
		onClick && onClick(id);
	}, [id]);

	if (size)
		return <ReactAvatar round size={size} name={name} {...restReactAvatar} />;

	return (
		<StyleAvatar $active={active} $isList={isList} onClick={onHandler}>
			<StyleAvatarContent>
				<ReactAvatar round size="100%" name={name} {...restReactAvatar} />
			</StyleAvatarContent>
		</StyleAvatar>
	);
};

export default React.memo(Avatar);
