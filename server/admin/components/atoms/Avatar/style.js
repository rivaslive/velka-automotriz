import styled from '@emotion/styled';

export const StyleAvatar = styled.div`
	width: 33px;
	height: 33px;
	padding: ${({ $active }) => ($active ? 2 : 0)}px;
	border-radius: 50%;
	border: ${({ $active }) =>
		$active ? '2px solid rgb(0, 82, 204)' : '2px solid transparent'};
	background: #ffffff;
	margin-left: ${({ $isList }) => ($isList ? -8 : 0)}px;
	z-index: ${({ $active }) => ($active ? 2 : 1)};
	transition: all 250ms;

	&:hover {
		transform: translateY(-3px);
		transition: all 250ms;
		z-index: 3;
	}
`;

export const StyleAvatarContent = styled.div({
	width: '100%',
	height: '100%',
	borderRadius: '50%',
	cursor: 'pointer',
	overflow: 'hidden',
});
