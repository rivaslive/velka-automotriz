import React from 'react';
import { StyleTextBarcode } from './style';

const BarcodeText = ({ children }) => (
	<StyleTextBarcode>{children}</StyleTextBarcode>
);

export default React.memo(BarcodeText);
