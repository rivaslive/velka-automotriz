import styled from '@emotion/styled';

export const StyleTextBarcode = styled.p`
	font-family: 'Libre Barcode 128 Text', cursive;
	font-size: 35px;
	margin-block-start: 0;
	margin-block-end: 10px;
`;
