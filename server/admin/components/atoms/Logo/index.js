import React from 'react';
import styled from '@emotion/styled';
import image from '../../../assets/images/velka.png';

const StyleLogo = styled.img`
	width: 350px;
	max-width: 100%;
	height: auto;
`;

export const Logo = () => <StyleLogo src={image} alt="Velka automotriz" />;
