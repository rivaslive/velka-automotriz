import React from 'react';
import { StyleMenuItem } from './style';

const MenuItem = ({ key, children, ...rest }) => (
	<StyleMenuItem key={key} {...rest}>
		{children}
	</StyleMenuItem>
);

export default MenuItem;
