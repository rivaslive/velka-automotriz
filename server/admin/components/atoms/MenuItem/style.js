import styled from '@emotion/styled';
import { Item as MenuItem } from 'rc-menu';

export const StyleMenuItem = styled(MenuItem)`
	cursor: pointer;
	padding: 7px 15px !important;
	white-space: nowrap;

	&:hover {
		background: rgb(244, 245, 247) !important;
	}
`;
