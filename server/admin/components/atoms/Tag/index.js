import React from 'react';
import { StyleTag } from './style';

const Tag = ({ children, variant }) => (
	<StyleTag $variant={variant}>{children}</StyleTag>
);

export default React.memo(Tag);
