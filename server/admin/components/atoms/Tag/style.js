import styled from '@emotion/styled';

const variants = {
	default: `
	  color: #52c41a;
    background: #f6ffed;
    border-color: #b7eb8f;
	`,
	success: `
		color: #52c41a;
		background: #f6ffed;
		border-color: #b7eb8f;
	`,
	error: `
		color: #f5222d;
		background: #fff1f0;
		border-color: #ffa39e;
	`,
	warning: `
		color: #fa8c16;
		background: #fff7e6;
		border-color: #ffd591;
	`,
	info: `
		color: #1890ff;
		background: #e6f7ff;
		border-color: #91d5ff;
	`,
};

export const StyleTag = styled.span`
	box-sizing: border-box;
	font-variant: tabular-nums;
	list-style: none;
	font-feature-settings: 'tnum';
	display: inline-block;
	height: auto;
	margin: 0 8px 0 0;
	padding: 5px 10px;
	font-size: 14px;
	font-weight: bold;
	line-height: 20px;
	white-space: nowrap;
	background: #fafafa;
	border: 1px solid #d9d9d9;
	border-radius: 2px;
	opacity: 1;
	transition: all 0.3s;
	${({ $variant }) => variants[$variant || 'default']}
`;
