import React from 'react';
import { InfoIcon } from '@primer/octicons-react';
import { WrapperEmpty } from './style';

const Empty = () => (
	<WrapperEmpty>
		<InfoIcon size={48} css={{ marginBottom: '0.5em' }} />
		Seleccione un usuario para ver su detalles
	</WrapperEmpty>
);

export default Empty;
