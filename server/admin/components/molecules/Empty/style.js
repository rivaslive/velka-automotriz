import styled from '@emotion/styled';
import { colors } from '@arch-ui/theme';

export const WrapperEmpty = styled.div`
	align-items: center;
	color: ${colors.N30};
	display: flex;
	flex-direction: column;
	font-size: 32px;
	justify-content: center;
	padding: 1em;
	text-align: center;
`;
