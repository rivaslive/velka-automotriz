import React from 'react';
import Menu from 'rc-menu';
import Dropdown from 'rc-dropdown';
import { useCalendar } from '../../../context/calendar.context';

// Components
import Avatar from '../../atoms/Avatar';
import MenuItem from '../../atoms/MenuItem';

// Styles
import { StyleContentFlex, ContentItem, Text } from './style';

const ListUsers = ({ data, handleUser }) => {
	const { show } = useCalendar();
	const moreUsers = data.length - show;

	const menu = React.useCallback(() => {
		return (
			<Menu onSelect={({ key }) => handleUser(key)}>
				{data.map(({ id, firstName, lastName, image }, i) => {
					if (i + 1 > show)
						return (
							<MenuItem key={id}>
								<ContentItem>
									<Avatar
										size="24px"
										maxInitials={2}
										src={image}
										name={`${firstName} ${lastName || ''}`}
									/>
									<Text>{`${firstName} ${lastName || ''}`}</Text>
								</ContentItem>
							</MenuItem>
						);
				})}
			</Menu>
		);
	}, [data]);

	if (!data && !data?.length) return <p>No hay clientes disponibles</p>;

	return (
		<StyleContentFlex>
			{data.map((avatar, index) => {
				if (index + 1 <= show)
					return (
						<Avatar
							isList
							id={avatar.id}
							key={avatar.id}
							onClick={handleUser}
							active={avatar.active}
							src={avatar.image}
							name={`${avatar.firstName || ''} ${avatar.lastName || ''}`}
						/>
					);
			})}
			{moreUsers > 0 && (
				<Dropdown overlay={menu()} trigger={['click']}>
					<Avatar
						isList
						fgColor="#000000"
						value={`+${moreUsers}`}
						color="rgb(223, 225, 230)"
					/>
				</Dropdown>
			)}
		</StyleContentFlex>
	);
	// return (
	// 	<div>
	// 		data
	// 		{`${data}`}
	// 		{data.map((avatar) => (
	// 			<p>{JSON.stringify(avatar)}</p>
	// 		))}
	// 	</div>
	// );
};

export default React.memo(ListUsers);
