import styled from '@emotion/styled';

export const StyleContentFlex = styled.div`
	margin-top: 5px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

export const ContentItem = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;

	.sb-avatar__text {
		span {
			font-size: 9px;
		}
	}
`;

export const Text = styled.span`
	margin-left: 10px;
	font-size: 14px;
	font-weight: normal;
`;
