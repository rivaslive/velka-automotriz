import React from 'react';
import moment from 'moment';
import { useQuery } from '@apollo/client';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import { formatCalendar, getColorEvent } from '../../../utils';
import { GET_BY_ID_CLIENT } from '../../../graphql/clients.query';
import { GET_ALL_PAYMENTS_FOR_CLIENT } from '../../../graphql/payments.query';

// Component
import { PageLoading } from '@keystonejs/app-admin-ui/components';
import BarcodeText from '../../atoms/BarcodeText';
import Tag from '../../atoms/Tag';

// styles
import { ContentItem, ContentUserInfo, StyleCalendarWrapper } from './style';

moment.updateLocale('es', { week: { dow: 1, doy: 4 } });
const localize = momentLocalizer(moment);

// Calendar components
const CalendarPayment = ({ active: id }) => {
	const { data: dataClient, loading: loadingClient } = useQuery(
		GET_BY_ID_CLIENT,
		{
			variables: { id },
		},
	);
	const { data: dataPayments, loading: loadingPayments } = useQuery(
		GET_ALL_PAYMENTS_FOR_CLIENT,
		{
			variables: { id },
		},
	);

	// fecha de pago del mes
	const paymentDate = React.useMemo(() => {
		if (dataClient?.Client) {
			const { Client } = dataClient;
			return moment(
				`${Client?.dayPayment || '1'}-${moment().format('M-YYYY')}`,
				'D-M-YYYY',
			);
		}
		return null;
	}, [dataClient?.Client]);

	// ultimo pago realizado
	const lastPayment = React.useMemo(() => {
		if (dataPayments?.allPayments) {
			const { allPayments } = dataPayments;
			return allPayments?.length ? moment(allPayments[0].createdAt) : undefined;
		}
		return undefined;
	}, [dataPayments?.allPayments]);

	// si se realizo o no el pago de este mes
	const isPaymentMade = React.useMemo(() => {
		if (lastPayment) {
			return lastPayment.format('MM-YYYY') === moment().format('MM-YYYY');
		}
		return false;
	}, [lastPayment]);

	// eventos de pago que se mostraran en el calendario
	const dataEvents = React.useMemo(() => {
		if (paymentDate) {
			if (!lastPayment) {
				return [
					{
						title: 'Día de pago',
						start: paymentDate,
						end: paymentDate,
					},
				];
			}
			return [
				{
					title: 'Pago realizado',
					start: lastPayment,
					end: lastPayment,
					resources: {},
				},
				{
					title: 'Día de pago',
					start: paymentDate,
					end: paymentDate,
				},
			];
		}
		return [];
	}, [paymentDate, lastPayment]);

	if (loadingClient || loadingPayments) return <PageLoading />;
	if (!dataClient) return <span>Error al recuperar el cliente</span>;

	const { Client } = dataClient;

	return (
		<StyleCalendarWrapper>
			<ContentUserInfo>
				<ContentItem>
					<BarcodeText>{Client.id}</BarcodeText>
				</ContentItem>
				<ContentItem>
					<b>Cliente: </b> {Client?.firstName} {Client?.lastName}
				</ContentItem>
				<ContentItem>
					<b>Último pago: </b>{' '}
					{lastPayment ? lastPayment.format('DD MMMM, YYYY') : '---'}
				</ContentItem>
				<ContentItem>
					<b>Próximo pago: </b>{' '}
					{moment(paymentDate).add(1, 'month').format('DD MMMM, YYYY')}
				</ContentItem>
				<ContentItem className="tag">
					<Tag variant={isPaymentMade ? 'success' : 'error'}>
						{isPaymentMade ? 'PAGADO' : 'PENDIENTE'}
					</Tag>
				</ContentItem>
			</ContentUserInfo>
			<Calendar
				events={dataEvents}
				endAccessor="end"
				localizer={localize}
				startAccessor="start"
				style={{ height: 720 }}
				eventPropGetter={getColorEvent}
				formats={formatCalendar}
			/>
		</StyleCalendarWrapper>
	);
};

export default React.memo(CalendarPayment);
