import styled from '@emotion/styled';

export const StyleCalendarWrapper = styled.div`
	margin-top: 30px;
	margin-bottom: 120px;

	//events
	.rbc-row-segment {
		margin-top: 8px;

		.rbc-event,
		.rbc-event-allday {
			border-radius: 4px;

			.rbc-event-content {
				font-size: 12px;
				font-weight: 500;
			}
		}
	}

	// toolbar
	.rbc-toolbar {
		margin-bottom: 30px;

		.rbc-toolbar-label {
			order: 1;
			font-weight: 500;
			font-size: 24px;
			padding: 5px 0;
		}

		.rbc-btn-group {
			order: 2;

			&:last-child {
				order: 3;
				display: none;
			}
		}
	}

	//Days header
	.rbc-row.rbc-month-header {
		.rbc-header {
			border-bottom: 0;
			padding-top: 8px;
			background: white;

			span {
				text-transform: uppercase;
				font-weight: 500;
				font-size: 11px;
				color: #70757a;
			}
		}
	}

	//calendar cells
	.rbc-day-bg,
	.rbc-off-range-bg {
		background: white;
	}

	.rbc-date-cell {
		color: #3c4043;
		font-size: 12px;
		font-weight: 500;
		padding-top: 5px;
		text-align: center;

		&.rbc-off-range {
			color: #a7adb4;
		}

		&.rbc-now.rbc-current {
			color: #3a86ff;

			a {
				border: 1px solid #3a86ff;
				border-radius: 50%;
				width: 20px;
				height: 20px;
				display: block;
				margin-right: auto;
				margin-left: auto;
			}
		}
	}

	.rbc-day-bg.rbc-today {
		background: white;
	}
`;

export const ContentUserInfo = styled.div`
	display: flex;
	justify-content: space-between;
	flex-direction: column;
	max-width: 100%;
	width: 350px;
	border-radius: 4px;
	padding: 20px;
	margin-bottom: 20px;
	background: white;
`;

export const ContentItem = styled.div`
	&.tag {
		margin: 20px 0;
	}
`;
