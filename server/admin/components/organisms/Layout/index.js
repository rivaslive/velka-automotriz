import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { KeystoneProvider } from '@keystonejs/apollo-helpers';

import client from '../../../apollo';

const Index = ({ children }) => {
	return (
		<ApolloProvider client={client}>
			<KeystoneProvider>{children}</KeystoneProvider>
		</ApolloProvider>
	);
};

export default Index;
