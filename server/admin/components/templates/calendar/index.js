import React, { useCallback, useEffect, useState } from 'react';
import 'rc-dropdown/assets/index.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { Container } from '@arch-ui/layout';
import { useQuery } from 'react-apollo';
import { PageTitle, SubtleText } from '@arch-ui/typography';
import { PageLoading } from '@keystonejs/app-admin-ui/components';

// queries
import { GET_ALL_CLIENTS } from '../../../graphql/clients.query';

// Component
import ListUsers from '../../molecules/ListUsers';
import { useCalendar } from '../../../context/calendar.context';
import CalendarPayment from '../../organisms/CalendarPayment';
import Empty from '../../molecules/Empty';

const CalendarTemplate = () => {
	const { data: dataClients, loading, refetch } = useQuery(GET_ALL_CLIENTS);
	const [data, setData] = useState([]);
	const { active, show, setActive } = useCalendar();

	// useEffect(() => {
	// 	return () => refetch();
	// }, []);

	useEffect(() => {
		if (data && data.length && !active) {
			setActive(data[0].id);
		}
	}, [data]);

	useEffect(() => {
		if (dataClients?.allClients) {
			setData(dataClients.allClients);
		}
	}, [dataClients]);

	const toggleAvatarUser = useCallback(() => {
		if (active) {
			let indexFind = 0;
			let find;
			// find and setIndex of array
			data.findIndex((i, index) => {
				if (i.id === active) {
					indexFind = index;
					find = { ...i, active: true };
				}
				return i.id === active;
			});
			// if index > 5 translate at position 1 in array
			if (indexFind >= show) {
				let newArray = data.filter((i) => i.id !== find.id);
				newArray = [
					find,
					...newArray.map((user) => {
						return { ...user, active: false };
					}),
				];
				return setData(newArray);
			}
		}
		return setData(
			data.map((user) => {
				if (user.id === active) return { ...user, active: true };
				return { ...user, active: false };
			}),
		);
	}, [active, data, setData]);

	// effects
	React.useEffect(() => {
		if (data.length > 0) {
			toggleAvatarUser();
		}
	}, [active]);

	const handleUser = useCallback(
		(id) => {
			setActive(id);
			toggleAvatarUser();
		},
		[active, data],
	);

	const getContent = React.useMemo(() => {
		if (active) return <CalendarPayment active={active} />;
		return <Empty />;
	}, [active]);

	if (loading) return <PageLoading />;

	return (
		<Container>
			{/* header */}
			<PageTitle>Calendario de pagos</PageTitle>
			<SubtleText>Seleccione un cliente para visualizar los pagos</SubtleText>

			{/* Users */}
			<ListUsers data={data} handleUser={handleUser} />

			{/* calendar */}
			{getContent}
		</Container>
	);
};

export default React.memo(CalendarTemplate);
