import React, { useContext, useState } from 'react';

export const CalendarContext = React.createContext({});

export const useCalendar = () => {
	return useContext(CalendarContext);
};

export const CalendarProvider = ({ children }) => {
	const [state, setState] = useState({
		active: null,
		show: 5,
	});

	const setActive = React.useCallback(
		(id) => setState((prev) => ({ ...prev, active: id })),
		[state.active],
	);

	return (
		<CalendarContext.Provider
			value={{
				...state,
				setActive,
			}}
		>
			{children}
		</CalendarContext.Provider>
	);
};
