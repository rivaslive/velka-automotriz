import gql from 'graphql-tag';

export const GET_ALL_CLIENTS = gql`
	query {
		allClients {
			id
			firstName
			lastName
			image
		}
	}
`;

export const GET_BY_ID_CLIENT = gql`
	query($id: ID!) {
		Client(where: { id: $id }) {
			id
			firstName
			lastName
			image
			dayPayment
		}
	}
`;
