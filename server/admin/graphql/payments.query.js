import gql from 'graphql-tag';

export const GET_ALL_PAYMENTS_FOR_CLIENT = gql`
	query allPaymentsById($id: [ID!]) {
		allPayments(where: { client: { id_in: $id } }, sortBy: createdAt_DESC) {
			payment
			invoice {
				publicUrl
			}
			description
			createdAt
		}
	}
`;
