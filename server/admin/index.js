import React from 'react';
import Calendar from './pages/calendar';
import { Logo } from './components/atoms/Logo';

export default {
	logo: Logo,
	pages: () => [
		{
			label: 'Usuarios',
			children: [
				{ listKey: 'User', label: 'Usuarios' },
				{ listKey: 'Client', label: 'Clientes' },
			],
		},
		{
			label: 'Administración',
			children: [
				{ listKey: 'Product', label: 'Productos' },
				{ listKey: 'Vehicle', label: 'Vehículos' },
				{ listKey: 'Payment', label: 'Pagos FORM' },
			],
		},
		{
			label: 'Calendario de pagos',
			path: 'calendar',
			component: Calendar,
		},
	],
};
