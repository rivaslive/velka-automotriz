import React from 'react';
import './global.css';
import 'rc-dropdown/assets/index.css';

// Component
import { CalendarProvider } from '../context/calendar.context';
import Layout from '../components/organisms/Layout';
import CalendarTemplate from '../components/templates/calendar';

export default () => (
	<CalendarProvider>
		<Layout>
			<CalendarTemplate />
		</Layout>
	</CalendarProvider>
);
