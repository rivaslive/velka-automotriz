export const getColorEvent = (event, start, end, isSelected) => {
	if (event.title !== 'Día de pago') {
		return { style: { backgroundColor: '#ff006e' } };
	}
	return { style: { backgroundColor: '#8338ec' } };
};

export const formatCalendar = {
	dateFormat: 'D',
};
