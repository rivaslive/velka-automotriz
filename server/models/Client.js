const { Relationship } = require('@keystonejs/fields');
const { text, state, number } = require('../utils/ksFields');
const { generateUriAvatar } = require('../utils/index');

module.exports = {
	fields: {
		firstName: { ...text, label: 'Nombres' },
		lastName: { ...text, label: 'Apellidos' },
		address: { ...text, label: 'Dirección' },
		department: { ...text, label: 'Departamento' },
		dayPayment: { ...number, label: 'Dia de pago' },
		vehicle: {
			type: Relationship,
			ref: 'Vehicle',
			many: true,
			label: 'Vehículos',
		},
		image: { ...text, label: 'Imagen (opcional generada por el sistema)' },
		state: {
			...state,
			label: 'Estado del cliente',
		},
	},
	labelResolver: (item) =>
		`${item?.firstName || ''} ${item?.lastName || ''} - ${
			item?.department || ''
		}`,
	hooks: {
		resolveInput: async ({
			operation,
			resolvedData,
			originalInput,
			existingItem,
		}) => {
			const isNew = operation === 'create';
			if (isNew || !originalInput?.image) {
				const image = generateUriAvatar(isNew ? originalInput : existingItem);
				return { ...resolvedData, image };
			}
			return resolvedData;
		},
	},
};
