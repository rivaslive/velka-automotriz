const { Relationship } = require('@keystonejs/fields');
const { text, state, decimal, image } = require('../utils/ksFields');
const { fileAdapter, fileAdapterGetHooks } = require('../utils/s3FileAdapter');

module.exports = {
	fields: {
		client: {
			type: Relationship,
			ref: 'Client',
			many: false,
			label: 'Cliente',
		},
		payment: { ...decimal, label: 'Pago' },
		description: { ...text, label: 'Datos extras (Opcional)' },
		invoice: {
			...image('file'),
			label: 'Factura',
		},
		state: {
			...state,
			label: 'Estado del pago',
		},
	},
	hooks: {
		afterDelete: fileAdapterGetHooks(fileAdapter, ['file']),
	},
	labelResolver: (item) => `${item?.client} - $${item?.payment}`,
};
