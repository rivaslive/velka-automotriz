const { text, state, decimal } = require('../utils/ksFields');

module.exports = {
	fields: {
		code: { ...text, label: 'Código de barras' },
		name: { ...text, label: 'Nombre' },
		description: { ...text, label: 'Descripción del producto' },
		price: { ...decimal, label: 'Precio de venta' },
		cost: { ...decimal, label: 'Precio de costo' },
		percentage: {
			...decimal,
			label: 'Porcentaje de ganancia',
			access: {
				read: ({ authentication }) => authentication.item.isAdmin,
				update: ({ authentication }) => authentication.item.isAdmin,
			},
		},
		state: {
			...state,
			label: 'Estado del producto',
		},
	},
	labelResolver: (item) => `${item?.name} - $${item?.price} - ${item?.code} `,
};
