const { Checkbox, Password, Text } = require('@keystonejs/fields');
const { text, state } = require('../utils/ksFields');

// Access control functions
const userIsAdmin = ({ authentication: { item: user } }) =>
	Boolean(user && user.isAdmin);
const userOwnsItem = ({ authentication: { item: user } }) => {
	if (!user) {
		return false;
	}
	return { id: user.id };
};

const userIsAdminOrOwner = (auth) => {
	const isAdmin = access.userIsAdmin(auth);
	const isOwner = access.userOwnsItem(auth);
	return isAdmin ? isAdmin : isOwner;
};

const access = { userIsAdmin, userOwnsItem, userIsAdminOrOwner };

module.exports = {
	fields: {
		name: { ...text, label: 'Nombre' },
		email: {
			type: Text,
			isUnique: true,
			label: 'Username o email',
		},
		isAdmin: {
			type: Checkbox,
			// Field-level access controls
			// Here, we set more restrictive field access so a non-admin cannot make themselves admin.
			adminDoc: 'Define si el usuario puede ingresar es administrador',
		},
		password: {
			type: Password,
		},
		state: {
			...state,
			label: 'Estado del usuario',
		},
	},
	// List-level access controls
	access: {
		read: access.userIsAdmin,
		update: access.userIsAdmin,
		create: access.userIsAdmin,
		delete: access.userIsAdmin,
		auth: true,
	},
};
