const { text, state } = require('../utils/ksFields');

module.exports = {
	fields: {
		brand: { ...text, label: 'Marca' },
		model: { ...text, label: 'Modelo' },
		color: { ...text, label: 'Color' },
		matricula: {
			...text,
			isUnique: true,
			label: 'Matricula',
		},
		state: {
			...state,
			label: 'Estado del vehiculo',
		},
	},
	labelResolver: (item) =>
		`${item?.brand || ''} ${item.model || ''},
		Color: ${item.color || '-'} - Matricula${item.matricula || '-'}`,
};
