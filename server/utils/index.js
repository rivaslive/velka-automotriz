const fs = require('fs');
const path = require('path');
const AVATAR_API_HOST = 'https://ui-avatars.com/api';

const bulkInstance = (dir) => {
	let files = fs.readdirSync(dir);
	// Remove hidden files
	files = files.filter((name) => !name.startsWith('.'));

	return files.map((file) => {
		const filePath = path.join(dir, file);
		const instance = require(filePath);
		const name = file.replace('.js', '');
		return name !== 'index' && { name: name, instance: instance };
	});
};

const generateUriAvatar = ({ firstName, lastName }) => {
	const url = `${AVATAR_API_HOST}/?name=${firstName || ''} ${
		lastName || ''
	}&background=random&rounded=true`;
	return encodeURI(url);
};

module.exports = {
	bulkInstance,
	generateUriAvatar,
};
