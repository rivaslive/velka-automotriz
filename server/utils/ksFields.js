const {
	Text,
	Relationship,
	Select,
	CalendarDay,
	// File,
	Checkbox,
	File,
	Decimal,
	Integer,
} = require('@keystonejs/fields');
const { Wysiwyg } = require('@keystonejs/fields-wysiwyg-tinymce');
const { fileAdapter, fileAdapterGetHooks } = require('./s3FileAdapter');

//
// General Fields
//
const name = { type: Text, isRequired: true };
const text = { type: Text };
const textArea = { type: Text, isMultiline: true };
const number = { type: Integer };
const decimal = { type: Decimal };
const checkbox = { type: Checkbox, defaultValue: false };

//
// Contents
//
const wysiwyg = {
	type: Wysiwyg,
	height: 400,
	editorConfig: {
		plugins: [
			'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			'table emoticons template paste help',
		],
		toolbar:
			'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			'forecolor backcolor emoticons | help',
		menu: {
			file: {
				title: 'File',
				items: 'newdocument restoredraft | preview | print ',
			},
			edit: {
				title: 'Edit',
				items: 'undo redo | cut copy paste | selectall | searchreplace',
			},
			view: {
				title: 'View',
				items:
					'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen',
			},
			insert: {
				title: 'Insert',
				items:
					'image link media template codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor toc | insertdatetime',
			},
			format: {
				title: 'Format',
				items:
					'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor | removeformat',
			},
			tools: {
				title: 'Tools',
				items: 'spellchecker spellcheckerlanguage | code wordcount',
			},
			table: {
				title: 'Table',
				items: 'inserttable | cell row column | tableprops deletetable',
			},
			help: { title: 'Help', items: 'help' },
		},
	},
};

//
// Selects
//

const state = {
	type: Select,
	options: ['active', 'deactivated'],
	defaultValue: 'active',
	dataType: 'string',
};

//
// Calendar
//
const date = {
	type: CalendarDay,
	// format: 'MM/DD/YYYY'
};

//
// Image
//
const image = (name = 'image', options = {}) => {
	return {
		...options,
		type: File,
		adapter: fileAdapter,
		hooks: {
			beforeChange: fileAdapterGetHooks(fileAdapter, [name]),
		},
	};
};

const user = {
	type: Relationship,
	ref: 'User',
};

module.exports = {
	text,
	textArea,
	name,
	state,
	wysiwyg,
	decimal,
	date,
	checkbox,
	number,
	user,
	image,
};
